// TicTacToe Game on React.JS
// Created by Dmitry Alexandrov
import { useState } from 'react'
import './game.css'

const Game = () => {
    let [board, setBoard] = useState([0,0,0,0,0,0,0,0,0])
    let [pl, setPl] = useState(1) // 'X'
    let [mess, setMess] = useState('Make turn')
    let [gameEnded, setGameEnded] = useState(false)

    function pressHandler(id) {
        if (gameEnded) return
        if (board[id] !== 0) return
        
        let b = board.map( (c) => c )
        b[id] = pl
        setBoard(b)

        //document.getElementById('' + id).innerText = (pl === 1) ? 'X' : 'O'
        if (hasWinner(b, pl)) {
            setGameEnded(true)
            setMess(`Congratulations, ${pl === 1 ? 'X' : 'O'}!`)
            // alert(`Congratulations, ${pl === 1 ? 'X' : 'O'}!`)
            return
        }
    
        if (isDraw(b)) {
            setGameEnded(true)            
            setMess('It is a draw.')
            // alert('It is a draw.')  
            return
        }

        setPl( (pl === 2) ? 1 : 2 )
        return    
    }

    function cell(i) {
        if (board[i] === 1) return 'X'
        else if (board[i] === 2) return 'O'
        return ''
    }

    function Player() {
        if (gameEnded) return (<div className='result'>{ mess }</div>)
        return (<div className='game-turn'>{ `Player: ${ (pl === 1) ? 'X' : 'O' }` }</div>)
    }

    function Cells() {
        const cells = board.map( (_, i) => 
            <button id={'' + i} className='cell' onClick={ () => { pressHandler(i) } }>
                { cell(i) }
            </button>
        )
        return (
            <div className="Game">
                { cells }
            </div>
        )
    }

    return (
        <div>
            <Menu />
            <Player />
            <Cells />
        </div>
    )
}

function hasWinner(b, p) {
    if (((b[0] === p) && (b[1] === p) && (b[2] === p)) ||
        ((b[3] === p) && (b[4] === p) && (b[5] === p)) ||
        ((b[6] === p) && (b[7] === p) && (b[8] === p)) ||
        ((b[0] === p) && (b[4] === p) && (b[8] === p)) ||
        ((b[2] === p) && (b[4] === p) && (b[6] === p)) ||
        ((b[0] === p) && (b[3] === p) && (b[6] === p)) ||
        ((b[1] === p) && (b[4] === p) && (b[7] === p)) ||
        ((b[2] === p) && (b[5] === p) && (b[8] === p))) return true
    return false
}

function isDraw(b) {
    for (let i = 0; i < 9; i++) if (b[i] === 0) return false
    return true
}

export default Game

const Menu = () => {
    return (
        <center>
            <a href="/" class="menu-new-game">New game</a>
        </center>
    )
}